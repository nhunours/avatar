/**
 *
 */

const fs = require('fs');
const isSvg = require("is-svg");
const libxmljs = require("libxmljs");

fs.readFile('./nhunours.svg', 'utf8', function(err, data) {
  if(err) {
    console.log('nhunours.svg not found');
    process.exit(1);
  } else {
    if(isSvg(data)) {
      try {
        const avatar = libxmljs.parseXml(data);
        if(avatar.errors.length === 0) {
          process.exit()
        } else {
          let i;
          for(i = 0; i < avatar.errors.length; i++) {
            console.log(avatar.errors[i]);
          }
          process.exit(4);
        }
      } catch(err) {
        console.log('Something is wrong in this SVG structure');
        process.exit(3);
      }

    } else {
      console.log('This is not an SVG content');
      process.exit(2);
    }
  }
});
