[project]: https://gitlab.com/nhunours/avatar
[build]: https://gitlab.com/nhunours/avatar/badges/master/pipeline.svg
[version]: https://img.shields.io/badge/version-1,0.0-blue.svg
[license]: https://img.shields.io/badge/license-MIT-green.svg

# Nhunours avatar

[![build status][build]][project]
[![version 0.1.0][version]][project]
[![license][license]](LICENSE.md)

![Nhunours avatar](nhunours.png)

## Running the tests

### Prerequisites

  - NodeJS
  - NPM

### Testing

```
npm run test
```

## Contributing

Please refer to [CONTRIBUTOR](CONTRIBUTOR.md).

## Authors &amp; contributors

[nhunours]: https://gitlab.com/nhunours/avatar

  - Nhu-Hoai Robert VO &mdash; Main developer &mdash; [@nhunours][nhunours]

## License &amp; copyright

This project is under [MIT license](LICENSE.md).

&copy;2018 Nhu-Hoai Robert VO
